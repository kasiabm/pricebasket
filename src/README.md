# PriceBasket

A shopping basket demo application that can price a basket of products
taking into account special offers. Written using TDD and JUnit4 (Gradle is being funny with Jupiter).

The entry point is PriceBasket that contains main method.
Information about products is encapsulated in Product object.
The products inventory is managed by ProductRepository;
the items are added to the Basket, which content is priced up by PriceCalculator
and totals displayed by Display, in this case ConsoleDisplay.

### Available products

- Apples at £1.00 per bag
- Bread at £0.80 per loaf
- Milk at £1.30 per bottle (let's assume it's a 1 pint bottle)
- Soup at £0.65 per tin

### Special offers

- 10% discount on apples
- Buy 2 tins of soup, get 1 loaf of bread at 1/2 price

### Run the program

Run main via console and input your shopping list in a form: `gradle run --args="apples milk bread"` etc.

### Assumptions and shortcuts

1. Items are entered as a string of items,
to purchase two bags of apples you enter a string: 'apples apples'.
2. Items are case insensitive.
3. For the purpose of this exercise a ProductRepository implementation
has been provided that initialises the repository to the above items.
4. For the purpose of this exercise a ConsoleDisplay has been provided
to display totals that outputs the totals to the console.
5. For the purpose of this exercise special offers have been coded into
PriceCalculator and ConsoleDisplay.
6. If entered item doesn't exist in stock, an exception is thrown that is handled by main method
and appropriate message displayed to the console;
for simplicity the totals are still displayed at £0.0 value.
7. Item price unit (bag, tin etc.) was not implemented.
8. There is no logging - the application is not complex enough to warrant overhead of logging the application flow.
9. Similarly to default SpringBoot applications an e2e tests were implemented to run main and test that it works.
