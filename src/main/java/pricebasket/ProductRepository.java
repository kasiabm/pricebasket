package pricebasket;

public interface ProductRepository {

  boolean contains(final String productName);

  Product getProduct(final String productName);
}
