package pricebasket;

public class PriceCalculator {

  private final ProductRepository productRepository;
  private final Basket basket;

  public PriceCalculator(final ProductRepository productRepository,
                         final Basket basket) {
    this.productRepository = productRepository;
    this.basket = basket;
  }

  public double basketSubTotal() {
    double subTotal = 0;
    for (final Product product : basket.getItems().keySet()) {
      subTotal += productRepository.getProduct(product.getProductName()).getPrice()
          * basket.getQuantity(product.getProductName());
    }
    return subTotal;
  }

  public double basketDiscounts() {
    double discounts = 0;
    if (basket.contains("Apples")) {
      discounts += basket.getQuantity("Apples") * 0.1;
    }
    if (basket.contains("Soup")) {
      if (basket.getQuantity("Soup") == 2 && basket.contains("Bread")) {
        discounts += 0.4;
      }
    }
    return discounts;
  }

  public double basketTotal() {
    return basketSubTotal() - basketDiscounts();
  }

  public Basket getBasketContent() {
    return basket;
  }
}
