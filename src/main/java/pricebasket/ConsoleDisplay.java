package pricebasket;

import java.util.ArrayList;
import java.util.List;

public class ConsoleDisplay implements PriceDisplay {

  private final List<String> availableDiscountTexts;
  private final PriceCalculator priceCalculator;

  public ConsoleDisplay(final PriceCalculator priceCalculator) {
    this.priceCalculator = priceCalculator;
    availableDiscountTexts = new ArrayList<>();
    initialiseAvailableDiscountTexts();
  }

  private void initialiseAvailableDiscountTexts() {
    availableDiscountTexts.add("Apples 10%% off: %dp");
    availableDiscountTexts
        .add("Buy 2 tins of soup, get a loaf of bread at half price: %dp");
  }

  @Override
  public void displayTotals() {
    System.out.println(String.format("Subtotal: £%.2f", priceCalculator.basketSubTotal()));
    if (priceCalculator.getBasketContent().contains("apples")) {
      System.out.println(String.format(availableDiscountTexts.get(0),
          priceCalculator.getBasketContent().getQuantity("apples") * 10));
    }
    if (priceCalculator.getBasketContent().contains("soup")
          && priceCalculator.getBasketContent().getQuantity("soup") == 2
          && priceCalculator.getBasketContent().contains("bread")) {
        System.out.println(String.format(availableDiscountTexts.get(1), 40));
      }
    System.out.println(String.format("Total price: £%.2f", priceCalculator.basketTotal()));
  }
}
