package pricebasket;

import java.util.HashMap;
import java.util.Map;

public class Basket {

  private final Map<Product, Integer> items;
  private final ProductRepository productRepository;

  public Basket(final ProductRepository productRepository) {
    items = new HashMap<>();
    this.productRepository = productRepository;
  }

  public void add(final String... productNames) throws IllegalArgumentException {
    for (final String productName : productNames) {
      if (productRepository.contains(productName)) {
        items.merge(productRepository.getProduct(productName), 1, Integer::sum);
      } else {
        throw new IllegalArgumentException(String.format("%s not available in stock", productName));
      }
    }
  }

  public Map<Product, Integer> getItems() {
    return items;
  }

  public boolean contains(final String productName) {
    boolean result = false;
    for (final Product product : items.keySet()) {
      if (productName.toLowerCase().equals(product.getProductName().toLowerCase())) {
        result = true;
        break;
      }
    }
    return result;
  }

  public int getQuantity(final String product) {
    return items.get(productRepository.getProduct(product));
  }
}
