package pricebasket;

import java.util.ArrayList;
import java.util.List;

public class ProductRepositoryImpl implements ProductRepository {

  private final List<Product> products;

  public ProductRepositoryImpl() {
    products = new ArrayList<>();
    initialiseProductRepository();
  }

  private void initialiseProductRepository() {
    products.add(new Product("apples", 1));
    products.add(new Product("bread", 0.8));
    products.add(new Product("milk", 1.3));
    products.add(new Product("soup", 0.65));
  }

  @Override
  public boolean contains(final String productName) {
    boolean result = false;
    for (final Product product : products) {
      if (productName.toLowerCase().equals(product.getProductName())) {
        result = true;
        break;
      }
    }
    return result;
  }

  @Override
  public Product getProduct(final String productName) {
    Product result = null;
    for (final Product product : products) {
      if (productName.toLowerCase().equals(product.getProductName())) {
        result = product;
        break;
      }
    }
    return result;
  }
}
