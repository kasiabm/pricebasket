package pricebasket;

public class PriceBasket {

  public static void main(String[] args) {
    final ProductRepository productRepository = new ProductRepositoryImpl();
    final Basket basket = new Basket(productRepository);
    final PriceCalculator priceCalculator = new PriceCalculator(productRepository, basket);
    final PriceDisplay priceDisplay = new ConsoleDisplay(priceCalculator);
    try {
      basket.add(args);
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }

    priceDisplay.displayTotals();
  }
}
