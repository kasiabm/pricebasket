package pricebasket;

import org.junit.Test;

/**
 * Tests functionality of PriceBasket
 */
public class PriceBasketEndToEnd {

  /**
   * Tests that main is run, items added to the basket and total without discounts
   * calculated and displayed
   */
  @Test
  public void shouldRunMainAddItemsToBasketAndDisplayTotalsWithoutDiscount() {
    PriceBasket.main(new String[]{"bread", "milk", "soup"});
  }

  /**
   * Tests that main is run, items added to the basked and total with applicable
   * discounts calculated and displayed
   */
  @Test
  public void shouldRunMainAddItemsToBasketAndDisplayTotalsWithDiscounts() {
    PriceBasket.main(new String[]{"apples", "bread", "milk", "soup", "Soup"});
  }

  /**
   * Tests that main is run and message displayed that item is not in stock
   */
  @Test
  public void shouldRunMainAddItemsToBasketAndDisplayMessageIfItemNotInStock() {
    PriceBasket.main(new String[]{"chives"});
  }
}
