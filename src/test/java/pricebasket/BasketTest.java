package pricebasket;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests functionality of Basket
 */
public class BasketTest {

  /**
   * Apples
   */
  private static final String APPLES = "Apples";
  /**
   * Soup
   */
  private static final String SOUP = "Soup";
  /**
   * System under test
   */
  private Basket basket;

  /**
   * Sets up test fixtures
   */
  @Before
  public void beforeTest() {
    final ProductRepository productRepository = new ProductRepositoryImpl();
    basket = new Basket(productRepository);
  }

  /**
   * Tests that a single item is added to the basket successfully
   */
  @Test
  public void shouldAddSingleItemToBasket() {
    basket.add(APPLES);

    assertTrue(basket.contains(APPLES));
  }

  /**
   * Tests that multiple items are added to the basket successfully
   */
  @Test
  public void shouldAddMultipleItemsToBasket() {
    basket.add(APPLES, SOUP);

    assertTrue(basket.contains(APPLES));
    assertTrue(basket.contains(SOUP));
  }

  /**
   * Tests that the same item has been added with correct quantity
   */
  @Test
  public void shouldAddSameItemTimesPassedAsArgument() {
    basket.add(APPLES, APPLES);

    assertEquals(2, basket.getQuantity(APPLES));
  }
}
