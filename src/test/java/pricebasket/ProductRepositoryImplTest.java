package pricebasket;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Tests functionality of ProductRepositoryImpl
 */
public class ProductRepositoryImplTest {

  /**
   * System under test
   */
  private ProductRepository productRepository;

  /**
   * Sets up test fixtures
   */
  @Before
  public void beforeTest() {
    productRepository = new ProductRepositoryImpl();
  }

  /**
   * Tests that true is returned if product exists in repository
   */
  @Test
  public void shouldReturnTrueIfProductInRepository() {
    assertTrue(productRepository.contains("apples"));
  }

  /**
   * Tests that false if returned if product doesn't exist in repository
   */
  @Test
  public void shouldReturnFalseIfProductNotInRepository() {
    assertFalse(productRepository.contains("beans"));
  }

  /**
   * Tests that null is returned if item doesn't exist in repository
   */
  @Test
  public void shouldReturnNullIfItemNotInRepository() {
    assertNull(productRepository.getProduct("chives"));
  }
}
