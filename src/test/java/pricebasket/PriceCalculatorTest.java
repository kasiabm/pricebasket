package pricebasket;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Tests functionality of PriceCalculator
 */
public class PriceCalculatorTest {

  /**
   * System under test
   */
  private PriceCalculator priceCalculator;
  /**
   * Basket
   */
  private Basket basket;

  /**
   * Sets up test fixtures
   */
  @Before
  public void beforeTest() {
    final ProductRepository productRepository = new ProductRepositoryImpl();
    basket = new Basket(productRepository);
    priceCalculator = new PriceCalculator(productRepository, basket);
  }

  /**
   * Tests that basket subtotal is calculated correctly
   */
  @Test
  public void shouldCalculateBasketSubTotal() {
    basket.add("apples");

    assertEquals(1, priceCalculator.basketSubTotal(), 0.01);
  }

  /**
   * Tests that discounts are calculated correctly
   */
  @Test
  public void shouldCalculateBasketDiscounts() {
    basket.add("apples", "bread", "bread", "soup", "Soup");

    assertEquals(0.5, priceCalculator.basketDiscounts(), 0.01);
  }

  /**
   * Tests that basket total is calculated correctly
   */
  @Test
  public void shouldCalculateBasketTotalWithoutDiscounts() {
    basket.add("milk", "bread", "Soup");

    assertEquals(2.75, priceCalculator.basketTotal(), 0.01);
  }
}
